﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Skyline.Models
{
    public partial class Transmission
    {
        public Transmission()
        {
            Cars = new HashSet<Car>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Car> Cars { get; set; }
    }
}
