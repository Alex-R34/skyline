﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace Skyline.Models
{
    public partial class SkylineContext : DbContext
    {
        private IConfiguration _configuration { get; }
        public SkylineContext()
        {
        }

        public SkylineContext(DbContextOptions<SkylineContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Engine> Engines { get; set; }
        public virtual DbSet<Transmission> Transmissions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(_configuration.GetConnectionString("Skyline"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp")
                .HasAnnotation("Relational:Collation", "Russian_Russia.1251");

            modelBuilder.Entity<Car>(entity =>
            {
                entity.ToTable("Car");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("uuid_generate_v4()");

                entity.Property(e => e.Engine).HasColumnName("engine");

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("model");

                entity.Property(e => e.Production)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("production");

                entity.Property(e => e.Transmission).HasColumnName("transmission");

                entity.Property(e => e.Weight).HasColumnName("weight");

                entity.HasOne(d => d.EngineNavigation)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.Engine)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("engine");

                entity.HasOne(d => d.TransmissionNavigation)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.Transmission)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("transmission");
            });

            modelBuilder.Entity<Engine>(entity =>
            {
                entity.ToTable("Engine");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("uuid_generate_v4()");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Transmission>(entity =>
            {
                entity.ToTable("Transmission");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("uuid_generate_v4()");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
