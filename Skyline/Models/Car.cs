﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Skyline.Models
{
    public partial class Car
    {
        public Guid Id { get; set; }
        public string Production { get; set; }
        public short Weight { get; set; }
        public string Model { get; set; }
        public Guid Engine { get; set; }
        public Guid Transmission { get; set; }

        public virtual Engine EngineNavigation { get; set; }
        public virtual Transmission TransmissionNavigation { get; set; }
    }
}
